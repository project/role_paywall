CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * How it works


INTRODUCTION
------------

The Role Paywall module allows site administrators to hide premium content from
users that don't have access to it. This is performed on a per-field basis so
the user will still see other parts of the page.

Access to content is determined based on access rules which are evaluated when
the entity is viewed in the full view mode. This module comes with a User Role
and Permission access rule and provides plugin support for additional rules to
be contributed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/role_paywall

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/role_paywall


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Installation via composer is recommended.
> composer require 'drupal/role_paywall:^2.1'


CONFIGURATION
-------------

Once the module is enabled, go to the settings form and select the entities that
contain premium content. This module supports all content entities that have
a canonical URL, except some core entities. After saving the
form, a tab will appear for each entity selected. Click on the relevant tab to
configure the settings for that entity.

**Note:** If the entity supports bundles, you will first need to select which
bundles the paywall configuration should apply to and then click save to show
the rest of the form.

The configuration form shows the access rule plugins available, the entity
bundles (if applicable), the 'premium content field' and the fields to hide.

Each access rule plugin provides its own settings form and will need to be
configured before it can be used. Once the plugin is configured, tick *Enabled*
to activate the access rule.

The paywall can be activated for all entities or for specific entities only
based on the value of a boolean field on the entity.

The list of fields to hide are taken from the Full or Default entity view modes.

To show a paywall block on the page, configure the block using the standard
Block Layout provided by Drupal. Under the block visibility settings, select
Role Paywall and then select the entity type this block relates to. It is also
recommended to configure a path in the Pages visibility.


HOW IT WORKS
------------

When the entity is viewed in the Full view mode and the paywall is configured,
access to the entity is evaluated by calling each enabled access rule plugin.
Access is assumed to be denied and fields will be hidden unless one of the
plugins returns TRUE.

The result is used to set the value of the #access option in the render array
for the fields selected to be hidden. Modules providing a custom entity could
render fields outside the standard process. This will mean the paywall is
unable to hide the value of that field. Bug reports of this nature should be
made with the module providing the custom entity.

If a paywall block is configured, it will be shown when access to the entity has
been denied. The fields to hide don't need to be selected in order for this to
work.
