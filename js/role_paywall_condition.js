/**
 * @file
 * Generate summary for role paywall on vertical tabs of block forms.
 */

 (function ($, Drupal) {

  'use strict';

  function checkboxesSummary(context) {
    // Determine if the condition has been enabled (the box is checked).
    var conditionChecked = $(context).find('[name="visibility[role_paywall][paywall]"]:checked').not('[value=none]').length;

    if (conditionChecked) {
      return Drupal.t("Only shown when the paywall has denied access to content");
    }
    else {
      return Drupal.t('Not restricted');
    }
  }

  /**
   * Provide the summary information for the block settings vertical tabs.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the block settings summaries.
   */
  Drupal.behaviors.blockSettingsSummaryRolePaywall = {
    attach: function () {
      $('[data-drupal-selector="edit-visibility-role-paywall"]').drupalSetSummary(checkboxesSummary);
    }
  };

}(jQuery, Drupal));
