<?php

namespace Drupal\role_paywall\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form used to display plugin settings.
 */
class RolePaywallPluginSettingsForm extends RolePaywallFormBase {

  /**
   * Injected Plugin Manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The Role Paywall Access Rule plugin being configured.
   *
   * @var \Drupal\role_paywall\Plugin\RolePaywallAccessRulePluginInterface
   */
  protected $plugin;

  /**
   * The Entity Type we are configuring this plugin for.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Constructs a RolePaywallSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The Role Paywall plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_manager, PluginManagerInterface $plugin_manager) {
    parent::__construct($config_factory, $entity_manager);
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.role_paywall_access_rule'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type = NULL, $id = NULL) {
    $this->entityType = $entity_type;
    $config_object = $this->config('role_paywall.plugin.' . $id . '.' . $entity_type);
    $plugin_config = (empty($config_object->get('configuration'))) ? [] : $config_object->get('configuration');
    $this->plugin = $this->pluginManager->createInstance($id, $plugin_config);

    $form = $this->plugin->buildConfigurationForm([], $form_state);

    $form['#prefix'] = '<div id="role-paywall-plugin-settings-form">';
    $form['#suffix'] = '</div>';

    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#role-paywall-plugin-settings-form', $form));
    }
    else {
      $response->addCommand(new HtmlCommand('.role-paywall-plugin-summary.' . str_replace('_', '-', $this->plugin->getPluginId()), $this->plugin->getConfigurationSummary()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->submitConfigurationForm($form, $form_state);

    $this->config('role_paywall.plugin.' . $this->plugin->getPluginId() . '.' . $this->entityType)
      ->set('configuration', $this->plugin->getConfiguration())
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_paywall_plugin_settings';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    $config_names = [];
    $definitions = $this->pluginManager->getDefinitions();
    $supported_entities = $this->getContentEntities();

    foreach ($supported_entities as $entity_type_id => $entity_label) {
      foreach ($definitions as $plugin_id => $definition) {
        $config_names[] = 'role_paywall.plugin.' . $plugin_id . '.' . $entity_type_id;
      }
    }
    return $config_names;
  }

}
