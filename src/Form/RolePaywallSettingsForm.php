<?php

namespace Drupal\role_paywall\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements role paywall configuration form.
 */
class RolePaywallSettingsForm extends RolePaywallFormBase {

  /**
   * Internal fields not need to be set with the paywall.
   */
  const NOT_APPLICABLE_FIELDS = [
    'status',
    'promote',
    'sticky',
    'revision_default',
    'revision_translation_affected',
    'default_langcode',
  ];

  /**
   * Stores locally the injected manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Injected Plugin Manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Entity type we are changing the settings for.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Constructs a RolePaywallSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity bundle info.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The Role Paywall plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, PluginManagerInterface $plugin_manager) {
    parent::__construct($config_factory, $entity_manager);
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('plugin.manager.role_paywall_access_rule'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_paywall_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $supported_entities = $this->getContentEntities();
    $config_names = [];

    foreach ($supported_entities as $entity_type_id => $entity_label) {
      $config_names[] = 'role_paywall.settings.' . $entity_type_id;
    }
    return $config_names;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type = NULL) {
    $this->entityType = $entity_type;
    $configuration = $this->config('role_paywall.settings.' . $entity_type);
    $plugin_definitions = $this->pluginManager->getDefinitions();

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $form['plugins'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Access Rule'),
        $this->t('Enabled'),
        $this->t('Summary'),
        $this->t('Operations'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('Sorry, there are no plugins found.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];

    $entity_plugin_settings = [];
    $entity_plugins = $configuration->get('plugins');
    if (!empty($entity_plugins)) {
      foreach ($entity_plugins as $entity_plugin) {
        $entity_plugin_settings[$entity_plugin['plugin_id']] = $entity_plugin;
      }
    }

    // Add weight and enabled status from settings and then sort by weight.
    foreach ($plugin_definitions as $plugin_id => $plugin) {
      $plugin_definitions[$plugin_id]['enabled'] = (empty($entity_plugin_settings[$plugin_id]['enabled'])) ? 0 : $entity_plugin_settings[$plugin_id]['enabled'];
      $plugin_definitions[$plugin_id]['weight'] = (empty($entity_plugin_settings[$plugin_id]['weight'])) ? 0 : $entity_plugin_settings[$plugin_id]['weight'];
    }
    uasort($plugin_definitions, 'Drupal\Component\Utility\SortArray::sortByWeightElement');

    foreach ($plugin_definitions as $plugin_id => $plugin) {
      $config_object = $this->config('role_paywall.plugin.' . $plugin_id . '.' . $entity_type);
      $plugin_config = (empty($config_object->get('configuration'))) ? [] : $config_object->get('configuration');
      $plugin_instance = $this->pluginManager->createInstance($plugin_id, $plugin_config);

      $form['plugins'][$plugin_id]['#attributes']['class'][] = 'draggable';
      $form['plugins'][$plugin_id]['#weight'] = $plugin['weight'];

      $form['plugins'][$plugin_id]['name'] = [
        '#markup' => $this->t('<strong>@label:</strong> @description', [
          '@label' => $plugin['label'],
          '@description' => $plugin['description'],
        ]),
      ];

      $form['plugins'][$plugin_id]['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $plugin['enabled'],
      ];

      $form['plugins'][$plugin_id]['summary'] = [
        '#prefix' => '<span class="role-paywall-plugin-summary ' . str_replace('_', '-', $plugin_id) . '">',
        '#markup' => $plugin_instance->getConfigurationSummary(),
        '#suffix' => '</span>',
      ];

      $form['plugins'][$plugin_id]['settings'] = [
        '#type' => 'link',
        '#title' => $this->t('Edit'),
        '#url' => Url::fromRoute('role_paywall.role_paywall_plugin_settings_form', [
          'entity_type' => $entity_type,
          'id' => $plugin_id,
        ]),
        '#attributes' => [
          'class' => ['use-ajax', 'button', 'button--small'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 800,
          ]),
        ],
      ];

      $form['plugins'][$plugin_id]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @label', ['@label' => $plugin['label']]),
        '#title_display' => 'invisible',
        '#default_value' => $plugin['weight'],
        '#attributes' => ['class' => ['table-sort-weight']],
      ];
    }

    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);

    // If entity only has one bundle then just mark it as active.
    if (count($bundles) == 1) {
      $bundle_id = key($bundles);
      $bundle = reset($bundles);
      $active_bundles = [$bundle_id => 1];
      $form['bundles'] = [
        '#type' => 'hidden',
        '#value' => $bundle_id,
      ];
    }
    else {
      $bundle_entity_type = $this->entityTypeManager->getDefinition($this->entityType)->get('bundle_entity_type');
      $bundle_entity_type_label = $this->entityTypeManager->getDefinition($bundle_entity_type)->get('label');

      $active_bundles = $configuration->get('bundles') ?: [];
      $options = [];
      foreach ($bundles as $bundle_id => $bundle) {
        $options[$bundle_id] = $bundle['label'];
      }
      $form['bundles'] = [
        '#type' => 'checkboxes',
        '#title' => $bundle_entity_type_label,
        '#options' => $options,
        '#default_value' => $active_bundles,
      ];

      if (empty($active_bundles)) {
        $vars = ['@label' => strtolower($bundle_entity_type_label)];
        $form['no_active_bundles'] = [
          '#markup' => $this->t('<strong>Note:</strong> Select a @label and click <em>Save configuration</em> to show the available fields.', $vars),
        ];
      }
    }

    foreach ($active_bundles as $bundle_id => $bundle_status) {
      if (empty($bundle_status)) {
        continue;
      }

      // Get list of fields from either full view mode or default.
      $entity_display = $this->entityTypeManager->getStorage('entity_view_display')->load($entity_type . '.' . $bundle_id . '.full');
      if (empty($entity_display)) {
        $entity_display = $this->entityTypeManager->getStorage('entity_view_display')->load($entity_type . '.' . $bundle_id . '.default');
      }
      $display_fields = array_keys($entity_display->get('content'));

      // Get fields for this entity type.
      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle_id);
      $hide_fields = [];
      $activate_paywall_field = [];

      foreach ($fields as $field_id => $field) {
        // Gather all fields that are in the display.
        if (in_array($field_id, $display_fields)) {
          $hide_fields[$field_id] = (string) $field->getLabel();
        }

        // Gather all boolean fields that are not in the exclusion list.
        if ($field->getType() === 'boolean' && !in_array($field_id, self::NOT_APPLICABLE_FIELDS)) {
          $activate_paywall_field[$field_id] = (string) $field->getLabel();
        }
      }

      // Add an option to not have a boolean field on each entity.
      $activate_paywall_field['role_paywall_no_field'] = 'All entities paywalled';
      $selected_activate_paywall_field = $configuration->get('activate_paywall_field');

      // Get 'pseudo-field' elements for the entity, add if included in display.
      $context = 'display';
      $extra_fields = $this->entityFieldManager->getExtraFields($entity_type, $bundle_id);
      $extra_fields = $extra_fields[$context] ?? [];

      foreach ($extra_fields as $field_id => $field) {
        if (in_array($field_id, $display_fields)) {
          $hide_fields[$field_id] = is_object($field['label']) ? $field['label']->render() : $field['label'];
        }
      }

      if (!isset($form['activate_paywall_field'])) {
        $form['activate_paywall_field'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Premium content field'),
        ];
        $form['hidden_fields'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Content to hide'),
        ];
      }

      $form['activate_paywall_field']['activate_paywall_field_' . $bundle_id] = [
        '#type' => 'radios',
        '#title' => $this->t('Field to mark @bundle behind the paywall', ['@bundle' => $bundles[$bundle_id]['label']]),
        '#options' => $activate_paywall_field,
        '#default_value' => empty($selected_activate_paywall_field[$bundle_id]) ? 'role_paywall_no_field' : $selected_activate_paywall_field[$bundle_id],
      ];
      $form['hidden_fields']['hidden_fields_' . $bundle_id] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Fields to hide in @bundle', ['@bundle' => $bundles[$bundle_id]['label']]),
        '#options' => $hide_fields,
        '#default_value' => empty($configuration->get('hidden_fields.' . $bundle_id)) ? [] : $configuration->get('hidden_fields.' . $bundle_id),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugins = [];
    $plugins_value = $form_state->getValue('plugins');
    foreach ($plugins_value as $plugin_id => $settings) {
      $plugins[] = ['plugin_id' => $plugin_id] + $settings;
    }

    $bundles = $form_state->getValue('bundles');
    $bundles = (is_array($bundles)) ? array_filter($bundles) : [$bundles => $bundles];
    $activate_paywall_field = [];
    $hidden_fields = [];

    foreach ($bundles as $bundle_id => $enabled) {
      $activate_paywall_field[$bundle_id] = $form_state->getValue('activate_paywall_field_' . $bundle_id) ?: '';
      $hidden_fields[$bundle_id] = array_filter($form_state->getValue('hidden_fields_' . $bundle_id) ?: []);
    }

    $this->config('role_paywall.settings.' . $this->entityType)
      ->set('bundles', $bundles)
      ->set('plugins', $plugins)
      ->set('activate_paywall_field', $activate_paywall_field)
      ->set('hidden_fields', $hidden_fields)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
