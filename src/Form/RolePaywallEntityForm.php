<?php

namespace Drupal\role_paywall\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements role paywall configuration form.
 */
class RolePaywallEntityForm extends RolePaywallFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_paywall_entity_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['role_paywall.entities'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->config('role_paywall.entities');
    $content_entities = $this->getContentEntities();

    $form['entity_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Paywalled Entities'),
      '#options' => $content_entities,
      '#default_value' => $configuration->get('entity_type') ?: [],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_type = array_filter($form_state->getValue('entity_type'));

    $this->config('role_paywall.entities')
      ->set('entity_type', $entity_type)
      ->save();
    drupal_flush_all_caches();
    parent::submitForm($form, $form_state);
  }

}
