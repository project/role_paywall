<?php

namespace Drupal\role_paywall\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements a base form for the role paywall configuration forms.
 */
abstract class RolePaywallFormBase extends ConfigFormBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RolePaywallFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get a list of all available supported content entities.
   *
   * This will exclude specific entity types that don't make sense to be
   * paywalled. This list mainly includes core entities.
   *
   * @return array
   *   Associative array - entity label keyed by the entity type ID.
   */
  protected function getContentEntities() {
    $content_entity_types = [];
    $entity_type_definations = $this->entityTypeManager->getDefinitions();

    // It doesn't make sense to apply a paywall to the entities below.
    $unsupported = [
      'block_content',
      'comment',
      'menu_link_content',
      'redirect',
      'shortcut',
      'taxonomy_term',
      'user',
    ];

    // Get list of content entities that have a canonical link.
    foreach ($entity_type_definations as $definition) {
      $id = $definition->get('id');
      $links = $definition->get('links');
      if ($definition instanceof ContentEntityType && !empty($links['canonical']) && !in_array($id, $unsupported)) {
        $content_entity_types[$definition->get('id')] = $definition->get('label');
      }
    }
    return $content_entity_types;
  }

}
