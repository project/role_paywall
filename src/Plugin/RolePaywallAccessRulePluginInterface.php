<?php

namespace Drupal\role_paywall\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an interface for plugins for the paywall.
 *
 * @see plugin_api
 */
interface RolePaywallAccessRulePluginInterface extends PluginInspectionInterface, PluginFormInterface {

  /**
   * Get the plugin configuration.
   *
   * @return array
   *   Array of settings for this plugin.
   */
  public function getConfiguration();

  /**
   * Get a summary of the plugin configuration.
   *
   * @return string
   *   A summary of the current settings for this plugin.
   */
  public function getConfigurationSummary();

  /**
   * Return the default configuration values for this plugin.
   *
   * @return array
   *   Associative array of default configuration values.
   */
  public function defaultConfiguration();

  /**
   * Perform required access checks and return TRUE to allow access to content.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user for this request.
   * @param string $entity_type
   *   The entity type being accessed.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity being accessed.
   */
  public function checkAccess(AccountInterface $account, $entity_type, ContentEntityInterface $entity);

}
