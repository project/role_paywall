<?php

namespace Drupal\role_paywall\Plugin\RolePaywallAccessRule;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\role_paywall\Plugin\RolePaywallAccessRuleBase;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a user role access rule for the Role Paywall.
 *
 * @RolePaywallAccessRule(
 *   id = "permission",
 *   label = @Translation("Permission"),
 *   description = @Translation("Evaluate access based on a permission"),
 * )
 */
class RolePaywallPermission extends RolePaywallAccessRuleBase implements ContainerFactoryPluginInterface {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a Permission object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PermissionHandlerInterface $permission_handler, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->permissionHandler = $permission_handler;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.permissions'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // The default configuration will be have the block hidden (0).
    return ['permission' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess(AccountInterface $account, $entity_type, ContentEntityInterface $entity) {
    return $account->hasPermission($this->configuration['permission']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm($form, FormStateInterface $form_state) {
    parent::buildConfigurationForm($form, $form_state);
    // Get list of permissions.
    $perms = [];
    $permissions = $this->permissionHandler->getPermissions();
    foreach ($permissions as $perm => $perm_item) {
      $provider = $perm_item['provider'];
      $display_name = $this->moduleHandler->getName($provider);
      $perms[$display_name][$perm] = strip_tags($perm_item['title']);
    }

    $form['permission'] = [
      '#type' => 'select',
      '#options' => $perms,
      '#title' => $this->t('Permission'),
      '#required' => TRUE,
      '#default_value' => empty($this->configuration['permission']) ? 'access paywalled content' : $this->configuration['permission'],
      '#description' => $this->t('Users with the selected permission will be able to access the content.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['permission'] = $form_state->getValue('permission');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationSummary() {
    if (empty($this->configuration['permission'])) {
      return $this->t('No permissions selected');
    }
    else {
      return $this->t('User with permission - @permission', [
        '@permission' => $this->configuration['permission'],
      ]);
    }
  }

}
