<?php

namespace Drupal\role_paywall\Plugin\RolePaywallAccessRule;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\role_paywall\Plugin\RolePaywallAccessRuleBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a user role access rule for the Role Paywall.
 *
 * @RolePaywallAccessRule(
 *   id = "user_role",
 *   label = @Translation("User Role"),
 *   description = @Translation("Evaluate access based on a role assigned to a user"),
 * )
 */
class RolePaywallUserRole extends RolePaywallAccessRuleBase implements ContainerFactoryPluginInterface {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new Role Paywall User Role instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess(AccountInterface $account, $entity_type, ContentEntityInterface $entity) {
    if (!empty($this->configuration['roles'])) {
      if (empty(array_intersect($account->getRoles(), $this->configuration['roles'])) || $account->isAnonymous()) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }
    else {
      return TRUE;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // The default configuration is no roles selected.
    return ['roles' => []] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    parent::buildConfigurationForm($form, $form_state);

    $roles = [];
    $roles_entities = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    foreach ($roles_entities as $role_id => $role) {
      $roles[$role_id] = $role->label();
    }

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles with premium access'),
      '#options' => $roles,
      '#required' => TRUE,
      '#default_value' => empty($this->configuration['roles']) ? [] : $this->configuration['roles'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $allowed_roles = array_filter($form_state->getValue('roles'));
    $this->configuration['roles'] = array_keys($allowed_roles);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationSummary() {
    if (empty($this->configuration['roles'])) {
      return $this->t('No roles selected');
    }
    else {
      $allowed_roles = $this->configuration['roles'];
      $roles_entities = $this->entityTypeManager->getStorage('user_role')->loadMultiple($allowed_roles);
      $labels = [];

      foreach ($roles_entities as $role) {
        $labels[] = $role->label();
      }

      return $this->t('User with role - @roles', [
        '@roles' => implode(', ', $labels),
      ]);
    }
  }

}
