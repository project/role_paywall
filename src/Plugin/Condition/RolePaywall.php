<?php

namespace Drupal\role_paywall\Plugin\Condition;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\role_paywall\RolePaywallManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Role Paywall' condition.
 *
 * This condition evaluates TRUE when the Role Paywall has denied access to
 * content on the page.
 *
 * @Condition(
 *   id = "role_paywall",
 *   label = @Translation("Role Paywall"),
 * )
 */
class RolePaywall extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The role paywall manager.
   *
   * @var \Drupal\role_paywall\RolePaywallManagerInterface
   */
  protected $paywall;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Creates a new Role Paywall Condition instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\role_paywall\RolePaywallManagerInterface $paywall
   *   The role paywall manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactory $config_factory,
    RouteMatchInterface $route_match,
    AccountInterface $current_user,
    RolePaywallManagerInterface $paywall,
    EntityTypeManager $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentRouteMatch = $route_match;
    $this->currentUser = $current_user;
    $this->paywall = $paywall;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('role_paywall'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // The default configuration will be have the block hidden (0).
    return ['paywall' => 'none'] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('role_paywall.entities');
    $entity_types = $config->get('entity_type');

    if (empty($entity_types)) {
      $form['paywall'] = [
        '#markup' => $this->t('No entity types have been selected in the paywall settings.'),
      ];
    }
    else {
      $options = [];
      foreach ($entity_types as $entity_type) {
        $entity_definition = $this->entityTypeManager->getDefinition($entity_type);
        $options[$entity_type] = $entity_definition->get('label');
      }
      $options['none'] = $this->t('None');

      $form['paywall'] = [
        '#title' => $this->t('Role Paywall Entity'),
        '#type' => 'radios',
        '#options' => $options,
        '#default_value' => empty($this->configuration['paywall']) ? 'none' : $this->configuration['paywall'],
        '#description' => $this->t('This block will be shown when access to an entity of this type is denied.'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Save the submitted value to configuration.
    $this->configuration['paywall'] = $form_state->getValue('paywall');

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if ($this->configuration['paywall']) {
      return $this->t('Only shown when the paywall has denied access to content');
    }
    else {
      return $this->t('Not Restricted');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if ($this->configuration['paywall'] == 'none') {
      return TRUE;
    }
    else {
      $entity = $this->getCurrentEntity();
      if (empty($entity)) {
        return FALSE;
      }
      else {
        $type = $entity->getEntityTypeId();
        $this->paywall->setEntityType($type);
        return $this->paywall->isAccessDenied($entity->id());
      }
    }
  }

  /**
   * Get a content entity from the parameters.
   *
   * @param string $slug
   *   Provide the slug to get a specific entity parameter (eg 'node').
   *
   * @return bool|\Drupal\Core\Entity\ContentEntityInterface
   *   The content entity or FALSE if no configured entity is found.
   */
  protected function getCurrentEntity($slug = NULL) {
    $entity = FALSE;
    if (empty($slug)) {
      $params = $this->currentRouteMatch->getParameters();

      foreach ($params as $param) {
        if ($param instanceof ContentEntityInterface) {
          $type = $param->getEntityTypeId();
          if ($type == $this->configuration['paywall']) {
            $entity = $param;
          }
        }
      }
    }
    else {
      $entity = $this->currentRouteMatch->getParameter($slug);
    }
    return $entity;
  }

  /**
   * Make sure the node ID is present in the cache tags.
   */
  public function getCacheTags() {
    $entity = $this->getCurrentEntity();
    if (empty($entity)) {
      return parent::getCacheTags();
    }
    else {
      $entity_id = $entity->getEntityTypeId();
      return Cache::mergeTags(parent::getCacheTags(), [$entity_id . ':' . $entity->id()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Get the default cache contexts.
    $contexts = parent::getCacheContexts();
    $entity = $this->getCurrentEntity();
    if (!empty($entity)) {
      $contexts[] = 'route';
      $contexts[] = 'user:' . $entity->id();
    }
    return $contexts;
  }

}
