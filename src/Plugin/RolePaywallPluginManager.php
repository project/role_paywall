<?php

namespace Drupal\role_paywall\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Role Payall Access Rule plugin manager.
 *
 * @see plugin_api
 */
class RolePaywallPluginManager extends DefaultPluginManager {

  /**
   * Constructs a Plugin Manager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/RolePaywallAccessRule',
      $namespaces,
      $module_handler,
      'Drupal\role_paywall\Plugin\RolePaywallAccessRulePluginInterface',
      'Drupal\role_paywall\Annotation\RolePaywallAccessRule',
    );
    $this->alterInfo('role_paywall_access_rule_info');
    $this->setCacheBackend($cache_backend, 'role_paywall_access_rule_info_plugins');
  }

}
