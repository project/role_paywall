<?php

namespace Drupal\role_paywall;

/**
 * Interface for the Role Paywall Manager.
 */
interface RolePaywallManagerInterface {

  /**
   * Set the entity type for this instances.
   *
   * @param string $entity_type
   *   The entity_type we are dealing with.
   */
  public function setEntityType($entity_type);

  /**
   * Registers an entity to be behind the paywall.
   *
   * @param int $id
   *   The entity id to add to the array.
   */
  public function addPaywallEntity($id);

  /**
   * Gets the array with entity ids of the entities behind the paywall.
   *
   * @return array
   *   Array with all entity ids.
   */
  public function getPaywallEntities();

  /**
   * Check if access to the entity has been denied.
   *
   * @param int $id
   *   The entity id to check.
   */
  public function isAccessDenied($id);

  /**
   * Get all the enabled access rule plugins.
   *
   * @return array
   *   An array of plugin objects indexed by plugin_id, sorted by weight.
   */
  public function getPlugins();

  /**
   * Gets all the entity bundles affected by the paywall.
   *
   * @return array
   *   An array with all the bundle keys.
   */
  public function getPaywallBundles();

}
