<?php

namespace Drupal\role_paywall\Controller;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ModalFormExampleController class.
 */
class RolePaywallPluginSettings extends ControllerBase {

  /**
   * Injected Plugin Manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The ModalFormExampleController constructor.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager.
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   */
  public function __construct(PluginManagerInterface $plugin_manager, FormBuilder $formBuilder) {
    $this->pluginManager = $plugin_manager;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.role_paywall_access_rule'),
      $container->get('form_builder'),
    );
  }

  /**
   * Callback for opening the modal form.
   */
  public function openModalForm(string $entity_type, string $id) {
    $response = new AjaxResponse();
    $plugin_form = $this->formBuilder->getForm('Drupal\role_paywall\Form\RolePaywallPluginSettingsForm', $entity_type, $id);
    $response->addCommand(new OpenModalDialogCommand('Access Rule Settings', $plugin_form, ['width' => '800']));
    return $response;
  }

}
