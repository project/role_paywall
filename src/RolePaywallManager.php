<?php

namespace Drupal\role_paywall;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Main management class for the Role Paywall.
 */
class RolePaywallManager implements ContainerInjectionInterface, RolePaywallManagerInterface {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $configuration;

  /**
   * Injected Plugin Manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * List of entities blocked, indexed by entity type and then id.
   *
   * @var array
   */
  private $paywallEntities = [];

  /**
   * The entity type we are dealing with.
   *
   * @var string
   */
  private $entityType;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.role_paywall_access_rule'),
      $container->get('current_user'),
    );
  }

  /**
   * Constructs a new RolePaywallManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory instance.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   Plugin manager for access rules.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PluginManagerInterface $plugin_manager, AccountInterface $current_user) {
    $this->configFactory = $config_factory;
    $this->pluginManager = $plugin_manager;
    $this->account = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityType($entity_type) {
    $this->entityType = $entity_type;
    $this->configuration = $this->configFactory->get('role_paywall.settings.' . $entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function addPaywallEntity($id) {
    $paywalled_entities = $this->getPaywallEntities();
    if (!in_array($id, $paywalled_entities)) {
      $this->paywallEntities[$this->entityType][] = $id;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPaywallEntities() {
    return (empty($this->paywallEntities[$this->entityType])) ? [] : $this->paywallEntities[$this->entityType];
  }

  /**
   * {@inheritdoc}
   */
  public function isAccessDenied($id) {
    return in_array($id, $this->getPaywallEntities());
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugins() {
    $plugins = [];
    $plugin_settings = $this->configuration->get('plugins');
    uasort($plugin_settings, 'Drupal\Component\Utility\SortArray::sortByWeightElement');

    foreach ($plugin_settings as $plugin) {
      if ($plugin['enabled']) {
        $plugin_id = $plugin['plugin_id'];
        $config_object = $this->configFactory->get('role_paywall.plugin.' . $plugin_id . '.' . $this->entityType);
        $plugin_config = (empty($config_object->get('configuration'))) ? [] : $config_object->get('configuration');
        $plugins[$plugin_id] = $this->pluginManager->createInstance($plugin_id, $plugin_config);
      }
    }

    return $plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaywallBundles() {
    return array_values($this->configuration->get('bundles') ?: []);
  }

  /**
   * Get the field which determines if the paywall is active.
   */
  public function getPaywallActiveField($bundle) {
    $active_field = $this->configuration->get('activate_paywall_field');
    if (!empty($active_field[$bundle])) {
      return $active_field[$bundle];
    }
  }

  /**
   * Get the fields which should be hidden when access denied.
   */
  public function getPaywallHiddenFields($bundle) {
    $hidden_fields = $this->configuration->get('hidden_fields');
    if (!empty($hidden_fields[$bundle])) {
      return $hidden_fields[$bundle];
    }
  }

  /**
   * Check if user is allowed to access this entity.
   */
  public function checkAccess($entity) {
    // If this is the main admin account, then always allow access.
    if ($this->account->id() == 1) {
      return TRUE;
    }

    // Check if an access check has already been performed and denied access.
    if ($this->isAccessDenied($entity->id()) === TRUE) {
      return FALSE;
    }

    // Get list of enabled access rules.
    $access_rules = $this->getPlugins();

    if (empty($access_rules)) {
      return TRUE;
    }
    else {
      foreach ($access_rules as $access_rule) {
        $result = $access_rule->checkAccess($this->account, $this->entityType, $entity);
        // If an access rule has granted access, don't run any further tests.
        if ($result === TRUE) {
          return TRUE;
        }
      }
      // If no rule allowed access to the content, add to list of entities.
      if (!$result) {
        $this->addPaywallEntity($entity->id());
        return FALSE;
      }
    }
  }

}
