<?php

namespace Drupal\role_paywall\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Role Paywall Access Rule annotation.
 *
 * Plugin Namespace: Plugin\RolePaywallAccessRule.
 *
 * Access Rule plugin implementations need to define a plugin definition array
 * through annotation.
 *
 * @see \Drupal\role_paywall\RolePaywallPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class RolePaywallAccessRule extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the access rule.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Description of access rule.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The name of the access rule class.
   *
   * @var string
   */
  public $class;

  /**
   * Settings for the access rule plugin.
   *
   * @var array
   */
  public $settings = [];

}
