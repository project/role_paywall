<?php

namespace Drupal\Tests\role_paywall\Functional;

/**
 * Test for the Role paywall content access.
 *
 * @group role_paywall
 */
class RolePaywallContentTest extends RolePaywallTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->createTestNodes();
  }

  /**
   * Sanity checks - access content without paywall.
   */
  public function testContentWithoutPaywall() {
    $this->drupalGet($this->testNodePublic->toUrl()->toString());
    $this->assertSession()->pageTextContains($this->testNodeTitle);
    $this->assertSession()->pageTextContains($this->testNodePremiumText);
  }

  /**
   * Activate paywall and make sure we can't access premium content.
   */
  public function testContentWithtPaywallNotAccess() {
    $this->setConfig();
    $this->drupalGet($this->testNodePublic->toUrl()->toString());
    $this->assertSession()->pageTextContains($this->testNodeTitle);
    $this->assertSession()->pageTextContains($this->testNodePremiumText);
    $this->drupalGet($this->testNodePremium->toUrl()->toString());
    $this->assertSession()->pageTextContains($this->testNodeTitle);
    $this->assertSession()->pageTextNotContains($this->testNodePremiumText);
    $this->assertSession()->pageTextContains('Powered by Drupal');
  }

  /**
   * Activate paywal, login as admin and make sure we can access content.
   */
  public function testContentWithtPaywallHaveAccess() {
    $this->setConfig();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->testNodePremium->toUrl()->toString());
    $this->assertSession()->pageTextContains($this->testNodeTitle);
    $this->assertSession()->pageTextContains($this->testNodePremiumText);
  }

}
