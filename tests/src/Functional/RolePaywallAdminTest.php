<?php

namespace Drupal\Tests\role_paywall\Functional;

/**
 * Test for the Role paywall administrative interface.
 *
 * @group role_paywall
 */
class RolePaywallAdminTest extends RolePaywallTestBase {

  /**
   * Use admin theme for testing of admin forms.
   *
   * @var string
   */
  protected $defaultTheme = 'seven';

  /**
   * Tests the configuration page access.
   */
  public function testEntityConfigPageAccess() {
    $this->drupalGet('/admin/config/content/role_paywall');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/content/role_paywall');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests the configuration page elements.
   */
  public function testEntityConfigPageElements() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/content/role_paywall');
    $this->assertSession()->elementExists('css', 'input[value=node]');
  }

  /**
   * Tests entity configuration is properly saved.
   */
  public function testEntityConfigPageSave() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/content/role_paywall');
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('edit-entity-type-node', 'node');
    $page->pressButton('Save configuration');
    $this->assertSession()->elementExists('css', 'input[value=node][checked=checked]');
    $entity_types = $this->config('role_paywall.entities')->get('entity_type');
    $this->assertArrayHasKey('node', $entity_types, 'Config does not contain entity type of node');
    $this->assertStringEndsWith('admin/config/content/role_paywall/node', $this->trimFilePath($page->findLink('Content')->getAttribute('href')));
  }

  /**
   * Tests node configuration form shows correct settings for config loaded.
   */
  public function testNodeConfigPage() {
    $this->setConfig();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/content/role_paywall/node');
    $this->assertSession()->elementExists('css', 'input[value=' . $this->activateFieldName . '][checked=checked]');
    $this->assertSession()->elementExists('css', 'input[value=' . $this->premiumFieldName . '][checked=checked]');
    $this->assertSession()->elementExists('css', 'input#edit-plugins-user-role-enabled[checked=checked]');
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('activate_paywall_field_article', 'role_paywall_no_field');
    $page->pressButton('Save configuration');
    $paywall_field = $this->config('role_paywall.settings.node')->get('activate_paywall_field');
    $this->assertArrayHasKey('article', $paywall_field, 'Config does not contain active paywall field for article bundle');
    $this->assertEquals('role_paywall_no_field', $paywall_field['article'], 'Config does not reflect updated active paywall field for article bundle');
  }

}
