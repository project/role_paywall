<?php

namespace Drupal\Tests\role_paywall\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Test setup for the Role paywall module.
 *
 * @group role_paywall
 */
abstract class RolePaywallTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'role_paywall',
    'field',
    'node',
    'block',
  ];

  /**
   * The field to test as a paywall content to hide.
   *
   * @var string
   */
  protected $premiumFieldName;

  /**
   * The field to test as a paywall activator.
   *
   * @var string
   */
  protected $activateFieldName;

  /**
   * An admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Role with premium access.
   *
   * @var string
   */
  protected $adminRole;

  /**
   * Test node Title.
   *
   * @var string
   */
  protected $testNodeTitle = 'test article title that is displayed';

  /**
   * Test node content of the premium field.
   *
   * @var string
   */
  protected $testNodePremiumText = 'Premium content that is displayed.';

  /**
   * Test premium node object.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $testNodePremium;

  /**
   * Test public node object.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $testNodePublic;

  /**
   * Default theme for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * @var bool
   */
  protected $strictConfigSchema = TRUE;

  /**
   * Performs the basic setup tasks.
   */
  public function setUp(): void {
    parent::setUp();

    // Creates content type.
    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'article',
        'name' => 'Article',
      ]);
    $type->save();
    $this->container->get('router.builder')->rebuild();

    // Creates role and user.
    $permissions = [
      'administer blocks',
      'administer site configuration',
      'access administration pages',
      'administer content types',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
    $admin_user_roles = $this->adminUser->getRoles();
    $this->adminRole = end($admin_user_roles);

    // Creates fields.
    $this->createFields();

    // Creates block.
    $this->container->get('module_installer')->install(['block']);
    $this->rebuildContainer();
    $this->container->get('router.builder')->rebuild();
    $this->drupalPlaceBlock('page_title_block', ['region' => 'content']);
    $this->drupalPlaceBlock('local_tasks_block', ['region' => 'content']);
    $this->drupalPlaceBlock('system_powered_by_block', [
      'region' => 'footer',
      'visibility' => [
        'role_paywall' => [
          'id' => 'role_paywall',
          'paywall' => 'node',
        ],
      ],
    ]);
  }

  /**
   * Creates a test fields.
   */
  protected function createFields() {
    // Boolean field to de/activate paywall on each node.
    $this->activateFieldName = mb_strtolower($this->randomMachineName());
    $type = 'boolean';
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $this->activateFieldName,
      'entity_type' => 'node',
      'bundle' => 'article',
      'type' => $type,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'article',
      'description' => 'Is premium',
      'required' => FALSE,
    ]);
    $field->save();

    // Text field to hide when the content is behind the paywall.
    $this->premiumFieldName = mb_strtolower($this->randomMachineName());
    $type = 'text';
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $this->premiumFieldName,
      'entity_type' => 'node',
      'bundle' => 'article',
      'type' => $type,
      'settings' => [],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'article',
      'description' => 'Premium content to hide',
      'required' => FALSE,
    ]);
    $field->save();

    $display = \Drupal::service('entity_display.repository')->getViewDisplay('node', 'article', 'default');
    $display->setComponent($this->premiumFieldName, ['region' => 'content'])->save();
    $display->setComponent($this->activateFieldName, ['region' => 'content'])->save();
  }

  /**
   * Sets a paywall configuration for article content.
   */
  protected function setConfig() {
    $this->config('role_paywall.entities')
      ->set('entity_type', ['node' => 'node'])
      ->save();

    $plugins = [
      [
        'plugin_id' => 'user_role',
        'enabled' => '1',
        'weight' => '0',
      ],
    ];

    $this->config('role_paywall.settings.node')
      ->set('bundles', ['article' => 'article'])
      ->set('plugins', $plugins)
      ->set('activate_paywall_field', ['article' => $this->activateFieldName])
      ->set('hidden_fields', ['article' => [$this->premiumFieldName => $this->premiumFieldName]])
      ->save();

    $this->config('role_paywall.plugin.user_role.node')
      ->set('configuration', ['roles' => [$this->adminRole]])
      ->save();
  }

  /**
   * Creates a test node.
   */
  protected function createTestNodes() {
    $config = [
      'title' => $this->testNodeTitle,
      'type' => 'article',
      $this->premiumFieldName => $this->testNodePremiumText,
    ];
    $this->testNodePublic = $this->drupalCreateNode($config + [
      $this->activateFieldName => FALSE,
    ]);
    $this->testNodePremium = $this->drupalCreateNode($config + [
      $this->activateFieldName => TRUE,
    ]);

  }

  /**
   * Removes base_url() and query args from file paths.
   *
   * @param string $path
   *   The path being trimmed.
   *
   * @return string
   *   The trimmed path.
   */
  protected function trimFilePath($path) {
    $base_path_position = strpos($path, base_path());
    if ($base_path_position !== FALSE) {
      $path = substr_replace($path, '', $base_path_position, strlen(base_path()));
    }
    $query_pos = strpos($path, '?');
    return $query_pos !== FALSE ? substr($path, 0, $query_pos) : $path;
  }

}
