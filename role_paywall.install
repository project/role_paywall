<?php

/**
 * @file
 * Contains install and upgrade routines for role_paywall.
 */

use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 */
function role_paywall_requirements($phase) {
  if ($phase == 'runtime') {
    $config = \Drupal::config('role_paywall.settings');
    $barrier = $config->get('barrier_block');

    if (!empty($barrier)) {
      $etm = \Drupal::entityTypeManager();
      $block = $etm->getStorage('block')->load($barrier);

      if (!empty($block)) {
        // Check if visibility settings for barrier block include role_paywall.
        $paywall = $block->getVisibilityCondition('role_paywall');
        if (!is_object($paywall) || empty($paywall->getConfiguration()['paywall'])) {
          $vars = [
            ':edit_url' => Url::fromRoute('entity.block.edit_form', ['block' => $barrier])->toString(),
          ];

          return [
            'role_paywall' => [
              'title' => t('Role Paywall'),
              'description' => t('Configuration for the barrier block has changed. Please <a href=":edit_url">edit the block settings</a> and check the new <strong>Role Paywall</strong> visibity option.', $vars),
              'severity' => REQUIREMENT_ERROR,
            ],
          ];
        }
      }
    }
  }
}

/**
 * Convert node settings to new entity specific format.
 */
function role_paywall_update_8001() {
  $config_factory = \Drupal::configFactory();
  $entity_config = $config_factory->getEditable('role_paywall.entities');
  $existing_config = $config_factory->get('role_paywall.settings');

  // If new config doesn't exist then create entities config with node enabled.
  if (empty($entity_config->get('entity_type'))) {
    $entity_config->set('entity_type', ['node'])->save();
  }

  $roles = $existing_config->get('roles');

  // If module was configured, copy existing settings and enable role plugin.
  if (!empty($roles)) {
    $config_factory->rename('role_paywall.settings', 'role_paywall.settings.node');
    $node_config = $config_factory->getEditable('role_paywall.settings.node');
    $plugins = [
      [
        'plugin_id' => 'user_role',
        'enabled' => TRUE,
        'weight' => '0',
      ],
    ];
    $node_config->set('plugins', $plugins);
    $node_config->save();

    $plugin_config = ['roles' => $roles];

    $config_factory->getEditable('role_paywall.plugin.user_role.node')
      ->set('configuration', $plugin_config)
      ->save();
  }

}
